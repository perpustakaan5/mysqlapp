#Pekan 3 Hari 3

from mysql.connector import connect
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:IsoEnv$A$1@localhost:3306/rentalfilm', echo=True)
Session = sessionmaker(bind=engine)
session = Session()


if session:
    print("Connection Success")


class Customers(Base):
    __tablename__ = 'customers'
    id = Column(Integer, primary_key=True)
    namaId = Column(String)
    namaLengkap = Column(String)
    email = Column(String)


class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='perpustakaan',
                              user='root',
                              password='IsoEnv$A$1')
        except Exception as e:
            print(e)

    def showUsers(self):
        try:
            crud_query = '''select * from customers;'''

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            return cursor.fetchall()
        except Exception as e:
            print(e)

    def showUserById(self, **params):
        try:
            userid = params['userid']
            crud_query = '''select * from customers;'''

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            return cursor.fetchall()[userid-1]
        except Exception as e:
            print(e)

    def insertUser(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into customers ({0}) values {1};'''.format(column, values)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.dataCommit()
        except Exception as e:
            print(e)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0], item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result

    def updateUserById(self, **params):
        try:
            userid = params['userid']
            values = self.restructureParams(**params['values'])
            crud_query = '''update customers set {0} where userid = {1};'''.format(values, userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def deleteUserById(self, **params):
        try:
            userid = params['userid']
            crud_query = '''delete from customers where userid = {0};'''.format(userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
