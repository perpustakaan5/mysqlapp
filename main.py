#Pekan 3 Hari 3

from models import Customers
from models import session


#daftar fungsi
def showUsers():
    result = session.query(Customers).all()
    for row in result:
        print(row.id, row.namaId, row.namaLengkap, row.email)


def showUserById(**params):
    print("data yang diminta berhasil ditampilkan")
    list_show = []
    for userid in params.values():
        list_show.append(session.query(Customers).filter(Customers.id == userid))

    for item in list_show:
        for row in item:
            print(row.id, row.namaId, row.namaLengkap, row.email)


def insertUser(**params):
    for item in params.values():
        session.add(Customers(namaId = item['username'], namaLengkap = item['namaLengkap'], email = item['email']))
    session.commit()


def updateUserById(**params):
    for item in params.values():
        result = session.query(Customers).filter(Customers.id == item['id']).one()
        for keyword in item.keys():
            if keyword == 'username':
                result.namaId = item['username']
            elif keyword == 'namaLengkap':
                result.namaLengkap = item['namaLengkap']
            elif keyword == 'email':
                result.email = item['email']
    session.commit()
    print("data berhasil diubah")


def deleteUserById(**params):
    for userid in params.values():
        session.delete(session.query(Customers).filter(Customers.id==userid).one())
    session.commit()
    print("data berhasil dihapus")


#panggil fungsi
if __name__ == "__main__":


    #menampilkan semua data
    showUsers()
    print("seluruh data berhasil ditampilkan")


    #menampilkan data yang diminta saja
    print('\n')
    print('-'*60)
    dataminta = {'1': 2, '2': 3}
    showUserById(**dataminta)
    print("data yang diminta berhasil ditampilkan")


    #menambah data
    print('\n')
    print('-' * 60)
    datatambah = {
        '1': {'username' : 'RK',
        'namaLengkap' : 'Ravi Kumar',
        'email' : 'ravi@gmail.com'}
    }
    insertUser(**datatambah)
    showUsers()
    print("data berhasil ditambah")


    # mengubah data
    print('\n')
    print('-' * 60)
    dataubah = {
        '1': {'id' : 6,
        'username' : 'RK'}
    }
    updateUserById(**dataubah)
    showUsers()
    print("data berhasil diubah")

    #menghapus data
    print('\n')
    print('-' * 60)
    datahapus = {
        '1': 6
    }
    deleteUserById(**datahapus)
    showUsers()
    print("data berhasil dihapus")